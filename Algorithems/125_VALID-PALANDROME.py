#!/usr/local/bin/python3
# 125  Valid Palindrome
import re

# My Solution
def isPalindrome(s):
    
    s = "".join(s.split())
    s = re.sub(r'[^\w\s]','',s)
    s = s.lower()
    return s[::-1] == s

# Other solution
def isPalindrome1(s):
    start = 0;
    end = len(s)-1
    while start<end:
        while start<end and not s[start].isalnum():
            start +=1
        while start<end and not s[end].isalnum():
            end -=1
        if start<end and s[start].lower() != s[end].lower():
            return False
        start +=1
        end -= 1
    return True



if __name__ == "__main__":
    p = "A but tuba"
    p2 = "A man, a plan, a canal: Panama"
    p3 = ""
    print(isPalindrome(p3))
