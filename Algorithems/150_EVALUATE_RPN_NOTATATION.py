#!/usr/local/bin/python2.7
# 150 Evaluate reverse Polish Notation
# test cases
exp1 = ["2", "1", "+", "3", "*"]
exp2 = ["4", "13", "5", "/", "+"]
exp3 = ["18"]
exp4 = ["4", "0", "/"]
exp5 = []
exp6 = ["-3", "-1", "/"]
exp7 = ["2", "3", "-4", "/", "+" ]
exp8 = ["3", "5", "+", ["2", "3", "+"]]  # 13
exp9 = ["10","6","9","3","+","-11","*","/","*","17","+","5","+"]  # 22

# my initial solution 
def evalRPN_0(expression):
    tokens = list()
    for token in expression:
        if token in ['-', '+', '*', '/']: # operatrors
            op1 = tokens.pop()
            op2 = tokens.pop()
            if token =='-':  result = op2 - op1
            if token == '+': result = op2 + op1
            if token == '*': result = op2 * op1
            if token == '/': result = abs(op2) // abs(op1) if op2 ^ op1 >= 0 else -(abs(op2) // abs(op1))
            tokens.append(result)
        else: 
            tokens.append(int(token)) 
    return tokens[0]
 
 
# other solution(s)
# mach7  which works!
def evalRPN_1(expression):   
    operands = list()
    operators = {'+' : (lambda x, y : x + y), 
                 '-' : (lambda x, y : x - y), 
                 '*' : (lambda x, y : x * y), 
                 '/' : (lambda x, y : abs(x) // abs(y) if x ^ y >= 0 else -(abs(x) // abs(y)))}
    for token in expression:
        if token in operators:
            second = operands.pop()
            first = operands.pop()
            operands.append(operators[token](first, second))
        else:
            operands.append(int(token))
    return operands[0]


    print(evalRPN_0(exp9))
    print(evalRPN_1(exp9))


    
    