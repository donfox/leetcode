#!/usr/local/bin/python3

# 151 Reverse Words in a String 
#
# Given an input string, reverse the string word by word.
# Example:
#  Given s = "the sky is blue",
#  return "blue is sky the".

def reverseWords(s):
    solution = []
    inWord = False
    for i in range(0, len(s)):
        if (s[i]==' ' or s[i]=='\t') and inWord:
            inWord = False
            solution.insert(0, s[start:i])
            solution.insert(0, ' ')
        elif not (s[i]==' ' or s[i]=='\t' or inWord):
            inWord = True
            start = i
    if inWord:
        solution.insert(0, s[start:len(s)])
        solution.insert(0, ' ')
    if len(solution)>0:
        solution.pop(0)
    return ''.join(solution)


if __name__ == "__main__":
    print(reverseWords("the sky is blue"))
    print(reverseWords(" "))
    print(reverseWords("Don Fox"))
    print(reverseWords("0000 1111 2222"))
    print(reverseWords(""))