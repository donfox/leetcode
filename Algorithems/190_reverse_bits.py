#!/usr/local/bin/python3.6
# 190. Reverse Bits
# Reverse bits of a given 32 bits unsigned integer.

# For example, given input 43261596 (represented in binary as 
# 00000010100101000001111010011100), return 964176192 (represented in binary 
# as 00111001011110000010100101000000).

input =  43261596
ret =  964176192

import ctypes

def reverseBits(n):
    n = ctypes.c_uint32(n).value    # convert number to unsigned int
    bit_str = bin(n)[2:].zfill(32)  # convert to bit string
    binary_str = bit_str[::-1]      # reverse bit string
    # convert reversed bit string to decimal
    decimal = 0
    for digit in binary_str:
        decimal = decimal*2 + int(digit)
    return decimal

def reverseBits2(n):
    ans = 0
    for i in range(32):
        ans <<= 1
        ans |= n & 1
        n >>= 1
    return ans

if __name__ == '__main__':
    print(reverseBits(int6))
    print(reverseBits2(int6))
    