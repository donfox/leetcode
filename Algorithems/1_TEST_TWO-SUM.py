import unittest
from 1_TWO_SUM import two_sum

class twoSum_test_case(unittest.TestCase):
    
    def setUp(self):
        pass
        
    def test_twoSum(self):
        self.assertEqual(twoSum([2, 7, 11, 15,1,0],9), [0, 1])
        
if __name__ == '__main__':
    unittest.main()