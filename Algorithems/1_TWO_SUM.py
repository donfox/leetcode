#!/usr/local/bin/python3

# 1. twoSum
# Given an array of integers, return indices of the two numbers that add up
# to a target. You may assume that each input would have one solution.
#
# Difficulty: Easy
# ------------------------------------------------------------------test data
num_array1 = [2, 7, 11, 15, 1,0] 
target1 = 9
num_array2 = [3, 2, 4]
target2 = 6
num_array3 = [1,3,0,7,9]
target3 = 10
num_array4 = [1,2,3,4,5,6,7,8,9,0]
target4 = 9
num_array5 = [0,1,2,3,7,0]
target5 = 0
num_array6 = []                     # empty list
target6 = 1
num_array7 = [2,7,3,1]              # no addends for the given sum
target7 = 11
num_array8 = [0,4,3,0]
target8 = 0

# -----------what I submitted
def two_sum(array, target):
    """
    1.Leetcode 
    twoSum 
    Given an array of integers, return indices of the two numbers that
    add up to a specific target.
    """
    def twoSum(num_array, target):
       indx_Dict = {}
       for indx in range(0, len(num_array)):
           if target - num_array[indx] in indx_Dict:
               return [indx_Dict[target-num_array[indx]], indx]
               
           indx_Dict[num_array[indx]]=indx


#---------------------------------------------------------------other solutions
def two_sum1(nums, target):
    x = 0
    for i in range(0,len(nums)):
        while x <= len(nums):
            x =+ 1
            if nums[i] + nums[x] == target:
                return ("Found!, Indices: ", i, x, "Numbers: ", nums[i] , nums[x], "Target ", target)
            elif x == len(nums) :
                break
            else:
                x += 1


if __name__ == "__main__":
    
    print(two_sum1(num_array8, target8))

    