#!/usr/local/bin/python3.6
# 20 Valid Parentheses
# Given a string containing just the characters '(', ')', '{', '}', '[' and ']',
# determine if the input string is valid.
#
# The brackets must close in the correct order, "()" and "()[]{}" are all valid
# but "(]" and "([)]" are not.
#

# test cases

# balanced
v0 = "()"
v1 = "((()))[][[[]]]{{{{}}}}"
v2 = "(()()()())"
v3 = "(((())))"
v4 = "(()((())()))"

# not balanced
f0 = "((((((())"
f1 = "()))"
f2 = "(()()(()"
f3 = "{{{[[[}}]]]}"

parens = [v0, v1, v2, v3, v4, f0, f1, f2, f3]

# My Version
from pythonds.basic.stack import Stack

def isValid(sym_str):
    print(sym_str)
    stack = list()
    for indx in range(len(sym_str)):
        if sym_str[indx] == '(' or \
            sym_str[indx] == '[' or \
            sym_str[indx] == '{':
            stack.append(sym_str[indx])
        else:
            if len(stack) == 0:
                return False
            curr_open_sym = stack.pop()
            if (sym_str[indx]==')' and curr_open_sym !='(') or \
               (sym_str[indx]==']' and curr_open_sym !='[') or \
               (sym_str[indx]=='}' and curr_open_sym !='{'):
                return False 

    return len(stack) == 0


if __name__ == "__main__":
    
     for string in parens:
         print(isValid(string))