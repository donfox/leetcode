#!/usr/local/bin/python3.6

# 237. Delete Node in a Linked List
#        Write a function to delete a node (except the tail) in a singly linked
#        list, given only access to that node.Supposed the linked list is 1 -> 2
#        -> 3 -> 4 and you are given the third node with value 3, the linked
#        list should become 1 -> 2 -> 4 after calling your function.   

class Node(object):
    def __init__( self, x):
        self.data = x
        self.next = None
                 
class LnkList(object):
        
    def __init__( self, head = None ):
        self.head = None
                   
    def insert( self, data ):
        new_node = Node(data)
        if self.head == None :
            self.head = new_node
        else:
            new_node.next = self.head
            self.head = new_node
            
    def list_print(self):
            node = self.head
            while node:
                print (node.data)
                node = node.next
     
    def deleteNode(self, node):
        node.val = node.next.val
        node.next = node.next.next
                

    def search(self, target_data):
        node = self.head
        while node.next is not None:
            if node.data == target_data:
                return True
            else:
                node = node.next
        return False

             
if __name__ == "__main__":
    
    dataLst = ['1', '2', '3', '4']
    
    Ll = LnkList()
    for i in dataLst:
        Ll.insert(i)   
    Ll.list_print()
    print(Ll.search('3'))
#    print(Ll.size())
  
    



     
            
            
            
        

        
        