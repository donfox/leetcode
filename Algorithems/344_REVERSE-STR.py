#!/usr/local/bin/python3.6
# 344. Reverse String
# Write a function that takes a string as input and returns the string reversed.

def reverseString(s):
    print(s)
    return s[::-1]


if __name__ == "__main__":
    s = 'hello don @ 123'
    print(reverseString(s))