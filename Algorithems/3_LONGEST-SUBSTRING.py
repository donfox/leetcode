#!/usr/local/bin/python3.6
# 3. Longest Substring Without Repeating Characters - Medium
# Given a string, find the length of the longest substring without
# repeating characters.

# test cases
str1 = "abcabcbb"       # Answer is "abc" with length 3
str2 = 'stackoverflow'  # Output: "stackoverfl" with length 11
str3 = 'bbbbb'          # Output: 'b' with length 1
str4 = ''               # Output: '' with length 0
str5 = 'xxYzabczz'

# My Attempt
def lengthOfLongestSubstring(s):

    last_repeater = -1
    longest_sub = 0
    chars_indx = {}

    for i in range(len(s)):
        if s[i] in chars_indx and last_repeater < chars_indx[s[i]]:
            last_repeater = chars_indx[s[i]]
        if i - last_repeater > longest_sub:
            longest_sub = i - last_repeater
        chars_indx [s[i]] = i
    return longest_sub

if __name__ =="__main__":
    print(lengthOfLongestSubstring(str1))
   
    