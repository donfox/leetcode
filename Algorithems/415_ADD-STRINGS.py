#!/usr/local/bin/python3.6
# 415 Add Strings
# Given two non-negative integers num1 and num2 represented as string, return
# the sum of num1 and num2.

# 1.The length of both num1 and num2 is < 5100.
# 2.Both num1 and num2 contains only digits 0-9.
# 3.Both num1 and num2 does not contain any leading zero.
# 4.You must not use any built-in BigInteger library or convert the inputs to
#   integer directly.

def addStrings(num1, num2):
    """
    :type num1: str
    :type num2: str
    :rtype: str
    """
    def numericStr2int(num):
        ctr = i = 0
        for c in reversed(num):
            i += (ord(c) - 48) * (10**ctr)
            ctr += 1 
        return i
        
    if num1 == "0" and num2 =="0":
        return "0"
          
    if (len(num1) <= 5100) or len(num2) <= 5100 \
        and (num1.isdigit and num2.isdigit) \
        and (num1[0] != "0" or num2[0] != '0'):

        return str(numericStr2int(num1) + numericStr2int(num2))
        
        
def addStrings1(num1, num2): # Note:No
    """
    :type num1: str
    :type num2: str
    :rtype: str
    """
    result = []
    carry = 0
    idx1, idx2 = len(num1), len(num2)
    while idx1 or idx2 or carry:
        digit = carry
        if idx1:
            idx1 -= 1
            digit += int(num1[idx1])
        if idx2:
            idx2 -= 1
            digit += int(num2[idx2])
        carry = digit > 9
        result.append(str(digit % 10))
    return ''.join(result[::-1])
      
        

if __name__ == "__main__":
    ret = addStrings('0', '9')
    print(ret, type(ret))
    print(addStrings1('0', '9'))