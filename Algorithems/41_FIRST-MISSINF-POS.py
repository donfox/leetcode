#!/usr/local/bin/python3.6
# 41 First Missing Positive
# Given an unsorted integer array, find the first missing positive integer.
# [1,2,0] return 3
# [3, 4,-1, 1] return 2

def firstMissingPos(nums):
    print(nums)
    complete = list()
    small, large = 1, 1
    for n in nums:
        if n <= small:small = n
        if n >= large:large = n
   
    print(small, large)        
    for n in range(small, large+1):
        complete.append(n)
    print('complete', complete)
    for num in complete:
        if num > 0:
            if num not in nums:
                return num
    

if __name__ == "__main__":
    print(firstMissingPos([1,2,0]))