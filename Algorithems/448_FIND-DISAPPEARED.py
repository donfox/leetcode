#!/usr/local/bin/python3.6
# 448. Find All Numbers Disappeared in an Array
# some elements appear twice and others appear once.
# Input: [4,3,2,7,8,2,3,1]
# Output: [5,6]

# My solution
def findDisappearedNumbers(int_array):
    '''
    Array(list) of integers definition: 1 <= a[i] <= len(a)
    Find all the elements of [1, n] inclusive that do not appear in this array.
    '''
    # Check if int_array has element(s) values of least 1 and largest element
    # must be no larger than the length of array minus 1.    
    def findDisappearedNumbers(self, nums):
            if nums == []:
                return []
            small, large = 1, 1
            missing = list()

            for n in nums:
                if n <= small:
                    small = n
                if n >= large:
                    large = n
                
            if (large-small) > len(nums):
               return None

            for n in range(small, len(nums)):   
                if n not in nums:
                    missing.append(n)
            return missing
 
# Other solution
def findDisappearedNumbers1(nums):
    for i in range(len(nums)):
        index = abs(nums[i]) - 1
        nums[index] = -abs(nums[index])
    return [i + 1 for i in range(len(nums)) if nums[i] > 0]

if __name__ == "__main__":
    # test_cases
    #
    n_Array1 = [4, 3, 2, 7, 8, 2, 3, 1]
    n_Array2 = []
    n_Array4 = [1,1]  # output = [2]
    n_Array5 = [2,2]
    n_Array6 = [1,2]
    
    print(findDisappearedNumbers(n_Array5))
    print('----------------------------')
    print(findDisappearedNumbers1(n_Array5))

    
    