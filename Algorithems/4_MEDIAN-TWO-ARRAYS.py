#!/usr/local/bin/python3
import statistics as stat

# Problem:
# 	There are two sorted arrays nums1 and nums2 of size m and n respectively.
# 	Find the median of the two sorted arrays. The overall run time complexity should be O(log (m+n)).
#
# Easy SOLUTION
#
def isOdd(num):
    return (num / 2) != (num // 2)

nums1, nums2, nums3, nums4 = [1, 3], [2], [1, 2], [3, 4]
ar1 = [1, 12, 15, 26, 38]
ar2 = [2, 13, 17, 30, 45]

class Solution:
    from typing import List
    def findMedianSortedArrays(self, lst_a: List[int], lst_b: List[int]) -> float:
        lst_a.extend(lst_b)
        if isOdd(len(lst_a)):
            med = stat.median(lst_a)
            return med
        else:
            nums3
            med = stat.median(lst_a)
            return med


if __name__ == "__main__":
    S = Solution()
    med = S.findMedianSortedArrays(nums3, nums4)
    print(med)
    med = S.findMedianSortedArrays(nums1, nums2)
    print(med)
    med = S.findMedianSortedArrays(ar1, ar2)
    print(med)