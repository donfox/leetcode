#!/usr/local/bin/python3.7

import numpy as np
# 73. Set Matrix Zeroes
# Given a m x n matrix, if an element is 0, set its entire row and column to 0.
# Do it in place.  

matrix = np.array= ([
            [ 0, 2, 3, 4, 5, 6, 22, -2, 17],
            [ 1, 3, 4, 5, 6, 7, 12,  3,  2],
            [14, 1, 0, 7, 4, 0, 34, -6,  4],
            [ 1, 2, 3, 4, 5, 5, -9,  1,  2],
            [ 9, 2, 4, 3,-8, 6, -1,  6,  5]
         ])

def setZeroes(matrix):
    """
    :type matrix: List[List[int]]
    :rtype: void Do not return anything, modify matrix in-place instead.
    """
    cord_lst = []
    iter_num = 0
    for l in matrix:
        if 0 in l:
           for index, i in enumerate(l):
               l[index] = 0
           cord_lst.append([iter_num,l.index(0)])
        iter_num += 1
#    for c in cord_lst:
#        print(c)     
#    matrix[4][0] = 0
    

if __name__ == '__main__':
    setZeroes(matrix)    
    print(matrix)