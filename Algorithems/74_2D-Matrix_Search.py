#!/usr/local/bin/Python3.6
# 74. Search a 2D matrix
# Write an efficient algorithm that searches for a value in an m x n matrix. 
# This matrix has the following properties:

# 1.Integers in each row are sorted from left to right.
# 2.The first integer of each row is greater than the last integer of the 
# previous row. 

# Test cases
matrix = [
           [1, 3, 5, 7],
           [10, 11, 16, 20],
           [23, 30, 34, 50]
         ]
         
matrix2 = [
             [0, 2, 4, 6, 8, 10],
             [11, 12, 13, 14],
             [18, 21, 24, 27, 30, 33],
             [70, 80],
             [81, 82, 83, 84, 85]
          ]
           
def searchMatrix(matrix, target):
    """
    :type matrix: List[List[int]]
    :type target: int
    :rtype: bool
    """ 
    for l in matrix:
        found_flag = False
        if (l[0] < target and l[-1] > target):
            for i in l:
                if i == target:
                    found_flag = True
        return found_flag
         
if __name__ == '__main__':
    print(searchMatrix(matrix2, 3))