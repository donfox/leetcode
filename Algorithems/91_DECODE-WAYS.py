#!/usr/local/bin/python3

# 91. Decode Ways
# 
# http://massivealgorithms.blogspot.com/2014/06/leetcode-decode-ways-darrens-blog.html

# Given encoded message "12", it could be decoded as "AB" (1 2) or "L" (12).
# The number of ways decoding "12" is 2.
#
# Solution:
# The number of ways of decoding a string till the ith character
# is the number of ways of deciding till the previous one (if the ith character
# is not '0') plus the number of ways to decode till two before (if the last 
# two characters make a valid letter coding, i.e. from 10 till 26).

# test cases
message = '12379161212091932'
message2 = '123234567689966554332233543'
message3 = "10"
message4 = "100"
message5 = "0"
message6 = ""
message7 = '101'
message8 = '001'
message9 = '12300004'
message10 = '11111111111111111111-2-3-40'
message11 = '1'
message12 = '2002'
message13 = '0123'
message14 = '1234'


# The test cases make sense if you aren't allowed to ignore zeros.
# In other words, a 0 can be part of a letter representation (10->J, 20->T),
# but cannot represent a letter on its own (0->invalid).
#
#"10" can be J.
# "1" can be A.
# The 1 in "1XXXX" can denote an A if and only if the suffix also has a valid
# interpretation, which is not the case for "10".
# The same is true for "100". There is no valid interpretation for all the 
# digits. (1->A) leaves rest "00" which cannot be interpreted. (10->J) leaves
# rest "0" which cannot be interpreted.
#
alpha_numeric = {
    'A':1,  'B':2,  'C':3,  'D':4,  'E':5,  'F':6,  'G':7,  'H':8,  'I':9, 
    'J':10, 'K':11, 'L':12, 'M':13, 'N':14, 'O':15, 'P':16, 'Q':17, 'R':18,
    'S':19, 'T':20, 'U':21, 'V':22, 'W':23, 'X':24, 'Y':25, 'Z':26   
}

def decodeWays(msg):
    if not msg or len(msg) == 0: return 0 
    n = len(msg)
    cnt = [0] * (n+1)
    cnt[0] = 1
    for i in range(n+1):
        if msg[i-1] != "0":
            cnt[i] += cnt[i-1]
        if i != 1 and msg[i-2:i] < "27" and msg[i-2:i] > "09":
            cnt[i] += cnt[i-2]
    return cnt[len(msg)]
    
    
    
            
def numDecodings(s):  # Accepted 
    if s == "": return 0
    dp = [0 for x in range(len(s)+1)]
    dp[0] = 1
    for i in range(1, len(s)+1):
        if s[i-1] != "0":
            dp[i] += dp[i-1]
        if i != 1 and s[i-2:i] < "27" and s[i-2:i] > "09":
            dp[i] += dp[i-2]
    return dp[len(s)]
  
  
if __name__ == "__main__":
    message = message11
    print(decodeWays(message))
    print(numDecodings(message))
#------------------------------------------------------------------------------  
#
# # My Way
# def decode_ways(msg):
#     if not msg or len(msg) == 0: return 0
#     prev, curr = 0, 1
#     for i in range(len(msg)):
#         if not msg[i].isdigit():
#             return 0
#         temp = 0
#         if msg[i] != '0':
#             temp = curr
#         if i > 0 and msg[i-1]!= '0' and int(msg[i-1:i+1]) < 27:
#             temp += prev
#         prev = curr
#         curr = temp
#     return curr
#
# def numDecodings_4(s):  # Accepted
#     #dp[i] = dp[i-1] if s[i] != "0"
#     #       +dp[i-2] if "09" < s[i-1:i+1] < "27"
#     if s == "": return 0
#     dp = [0 for x in range(len(s)+1)]
#     dp[0] = 1
#     for i in range(1, len(s)+1):
#         if s[i-1] != "0":
#             dp[i] += dp[i-1]
#         if i != 1 and s[i-2:i] < "27" and s[i-2:i] > "09":
#             dp[i] += dp[i-2]
#     return dp[len(s)]
#
# def numDecodings_1(s):
#     if len(s) == 0:
#         return 0
#
#     previous = 0
#     actual = 1
#
#     for i in range(0,len(s)):
#         if not s[i].isdigit():
#             return 0
#         temp = 0
#         if s[i]!='0':
#             temp = actual
#         if i>0 and s[i-1]!= '0' and int(s[i-1:i+1])<27:
#             temp += previous
#         previous = actual
#         actual = temp
#     return actual
#
#
# def numDecodings_2(s):
#
#     if s=="" or s[0]=='0':
#         return 0
#
#     dp=[1,1]
#     for i in range(2,len(s)+1):
#         if 10 <=int(s[i-2:i]) <=26 and s[i-1]!='0':
#             dp.append(dp[i-1]+dp[i-2])
#         elif int(s[i-2:i])==10 or int(s[i-2:i])==20:
#             dp.append(dp[i-2])
#         elif s[i-1]!='0':
#             dp.append(dp[i-1])
#         else:
#             return 0
#     return dp[len(s)]
#
# def numDecodings_3(s):
#         if not s or s[0] == '0':
#             return 0
#         f = [1] + [0] * len(s)
#         for i in range(1,len(s)+1):
#             if s[i-1] != '0':
#                 f[i] += f[i-1]
#             if i > 1 and '09' < s[i-2] + s[i-1] <= '26':
#                 f[i] += f[i-2]
#         return f[-1]
#
#
# def numDecodings_5(s):
#     if not s or s[0] == '0':
#         return 0
#     dp = [1 for i in range(len(s) + 1)]
#     for i in range(1, len(s)):
#         dp[i + 1] = (dp[i] if s[i] != '0' else 0) + (dp[i - 1] if 10 <= int(s[i-1] + s[i]) <= 26 else 0)
#     return dp[-1]
#
# def numDecodings_6(s):
#     if not s or s[0] == '0':
#         return 0
#
#     decode_2 = 0
#     decode_1 = 1
#     decode_n = 1
#     # 10
#     for i in range(1, len(s)):
#         decode_n = 0
#         if s[i] > '0':
#             decode_n += decode_1
#
#         if '10' <= s[i-1:i+1] <= '26':
#             if i - 1 == 0:
#                 decode_n += 1
#             else:
#                 decode_n += decode_2
#
#         decode_2 = decode_1
#         decode_1 = decode_n
#
#     return decode_n
#
# def numDecodings_7(s):
#     if not s: return 0
#
#     dp = [0 for x in range(len(s) + 1)]
#     dp[0] = 1
#     dp[1] = 1 if 0 < int(s[0]) <= 9 else 0
#
#     for i in range(2, len(s) + 1):
#         if 0 < int(s[i-1:i]) <= 9:
#             dp[i] += dp[i - 1]
#         if s[i-2:i][0] != '0' and int(s[i-2:i]) <= 26:
#             dp[i] += dp[i - 2]
#     return dp[len(s)]
#
# def numDecodings_8(s):
#     v, w, p = 0, int(s>''), ''
#     for d in s:
#         v, w, p = w, (d>'0')*w + (9<int(p+d)<27)*v, d
#     return w


    