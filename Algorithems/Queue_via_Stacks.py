#!/usr/local/bin/python3.6
# 232 Implement Queue using Stacks
# Implement the following operations of a queue using stacks.
#
# push(x) -- Push element x to the back of queue.
# pop() -- Removes the element from in front of queue.
# peek() -- Get the front element.
# empty() -- Return whether the queue is empty.

# --                  Algorithm-psudocode
# Stack1, Stack2.
#
# Enqueue:
# Push el into stack1.
#
# Dequeue:
# While (!empty(Stack1))
#    el = Pop from stack1
#    Push el into stack2
#
# returnEl = Pop from Stack2
#
# While (!empty(Stack2))
#    el = Pop from stack2
#    Push el into stack1
#
# return returnEl

# My Implement basic algorithm 
class Stack:
     def __init__(self):
         self.items = []
     def Empty(self):
         return self.size() == 0
     def push(self, item):
         self.items.append(item)
     def pop(self):
         return self.items.pop()
     def peek(self):
         if len(self.items) > 0:
             return self.items[0]
#         return self.items[len(self.items)-1]
     def size(self):
         return len(self.items)

class Queue:
    def __init__(self):
        self.stack_1 = Stack()
        self.stack_2 = Stack()
        
    def enqueue(self, item):
        self.stack_1.push(item)

    def dequeue(self):
        if not self.stack_1.Empty():
            while self.stack_1.size() > 0:
                self.stack_2.push(self.stack_1.pop())
            res = self.stack_2.pop()

            while self.stack_2.size() > 0:
                 self.stack_1.push(self.stack_2.pop())
            return res
 

# class Queue(object):
#     def __init__(self):
#         self.instack, self.outstack = [], []
#     def enqueue(self,element):
#         self.instack.append(element)
#     def dequeue(self):
#         if not self.outstack:
#              while self.instack:
#                  self.outstack.append(self.instack.pop())
#         return self.outstack.pop()

class Queue1:
    # initialize your data structure here.
    def __init__(self):
        self.A, self.B = [], []

    # @param x, an integer
    # @return nothing
    def push(self, x):
        self.A.append(x)

    # @return nothing
    def pop(self):
        self.peek()
        return self.B.pop()
        
    # @return an integer
    def peek(self):
        if not self.B:
            while self.A:
                self.B.append(self.A.pop())
        return self.B[-1]
        
    # @return an boolean
    def empty(self):
        return not self.A and not self.B

if __name__ == "__main__":
    q = Queue()
    for i in range(0, 11):
        q.enqueue(str(i))
         
    for i in range(1, 11):
        print(q.dequeue())
    print('-------------------------------------')         
    q1 = Queue1()
    for i in range(0, 11):
        q1.push(str(i))

    for i in range(1, 11):
        print(q1.pop())
        