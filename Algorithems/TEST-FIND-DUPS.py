import unittest
from find_dups import findDups


class findDups_test_case(unittest.TestCase):
    
    def setUp(self):
        pass
    
    def test_findDups(self):
        self.assertEqual(findDups([4,3,2,7,8,2,3,1]), [2,3])

if __name__ == '__main__':
    unittest.main()