--  175.sql
--  COMBINE TWO TABLES
--  OUTER JOIN
--  Write a SQL query to provide the following information for each person in the
--  Person table, regardless if there is an address for each of those people:

DROP TABLE IF EXISTS Person;
CREATE TABLE Person (PersonId int, FirstName varchar(255), LastName varchar(255));
INSERT INTO Person (PersonId, LastName, FirstName) 
            VALUES ('1', 'Wang', 'Allen');

DROP TABLE IF EXISTS Address;
CREATE TABLE Address (AddressId int, PersonId int, City varchar(255), State varchar(255));
INSERT INTO Address (AddressId, PersonId, City, State) 
             VALUES ('1', '1', 'New York City', 'New York');

SELECT p.firstname, p.lastname, a.City, a.State 
FROM Person p left join Address a
ON p.Personid = a.Personid;