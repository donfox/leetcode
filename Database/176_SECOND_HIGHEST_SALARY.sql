-- 176.sql
-- SECOND HIGHEST SALARY
-- SUB SELECT
-- Get the second highest salary from the Employee table.

DROP TABLE IF EXISTS employee;
CREATE TABLE employee ( id int, salary int);

INSERT INTO employee(id, salary)
VALUES( 1, 100),
      ( 2, 200),
      ( 3, 300);

SELECT MAX(SALARY) as SecondHighestSalary
FROM Employee
WHERE Salary <> (SELECT MAX(Salary)
                     FROM Employee);


