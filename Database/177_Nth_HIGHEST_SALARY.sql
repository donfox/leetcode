-- 177.sql
-- Nth HIGHEST SALARY
-- STANDALONE FUNCTION
-- Get the nth highest salary from the Employee table.

DROP TABLE IF EXISTS employee;
CREATE TABLE employee (
    id int,
    salary int
);

INSERT INTO employee(id, salary)
VALUES( 1, 100),
      ( 2, 200),
	  ( 3, 300);

CREATE OR REPLACE FUNCTION getNthHighestSalary(n int) 
RETURNS INT AS $$
BEGIN
    RETURN ( SELECT DISTINCT Salary 
    	     FROM Employee 
             ORDER BY Salary DESC 
             LIMIT 1 OFFSET n);
END; $$

LANGUAGE plpgsql;

SELECT getNthHighestSalary(1);

