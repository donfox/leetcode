-- 178.sql
-- RANK SCORES
-- Count() used to generate RANK Column
--
-- Write a SQL query to rank scores. If there is a tie between two scores, 
-- both should have the same ranking. Note that after a tie, the next ranking
-- number should be the next consecutive integer value. In other words, there 
-- should be no "holes" between ranks.

CREATE TABLE If Not Exists Scores (Id int, Score DECIMAL(3,2));
Truncate table Scores;
INSERT INTO Scores (Id, Score) values ('1', '3.5');
insert into Scores (Id, Score) values ('2', '3.65');
insert into Scores (Id, Score) values ('3', '4.0');
insert into Scores (Id, Score) values ('4', '3.85');
insert into Scores (Id, Score) values ('5', '4.0');
insert into Scores (Id, Score) values ('6', '3.65');
	
-- 1.
SELECT s.Score, COUNT(t.Score) AS Rank 
FROM (SELECT DISTINCT Score FROM Scores) AS t, Scores AS s
WHERE s.Score <= t.Score
GROUP BY s.Id, s.Score
ORDER BY s.Score DESC;

-- 2.
SELECT Score, ( SELECT COUNT(*)   
                FROM ( SELECT DISTINCT Score S   
                       FROM Scores) AS Tmp
                WHERE S >= Score) AS Rank  
                FROM Scores  
                ORDER BY Score DESC;



