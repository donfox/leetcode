-- 180 Consecutive Numbers
-- Find all numbers that appear at least three times consecutively.
--

DROP TABLE IF EXISTS LOGS;
CREATE TABLE LOGS ( id  INT, Num INT );
-- TRUNCATE TABLE LOGS
INSERT INTO LOGS (id, Num)
          VALUES (1, 1),
                 (2, 1),
                 (3, 1),
                 (4, 2),
                 (5, 1),
                 (6, 2),
                 (7, 2);

-- Solution 2
SELECT DISTINCT l1.Num AS ConsecutiveNums
FROM LOGS l1, LOGS l2, LOGS l3  
WHERE l1.Id + 1 = l2.Id AND 
      l2.Id + 1 = l3.Id AND 
      l1.Num = l2.Num   AND 
      l2.Num = l3.Num  