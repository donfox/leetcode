-- 181.sql
-- EMPLOYEES EARNING MORE THAN THEIR MANAGERS
-- The Employee table holds all employees including their managers. 
-- Every employee has an Id, and there is also a column for the 

DROP TABLE IF EXISTS Employee ;
CREATE TABLE Employee (
    Id    INT,
    NAME  VARCHAR(20),
    Salary INT,
    ManagerId  INT
) ;

INSERT INTO Employee(Id, NAME, Salary, ManagerId)
VALUES (1, 'Joe',    7000, 3),
       (2, 'Henery', 8000, 4),
       (3, 'Sam',    6000, NULL),
       (4, 'Max',    9000, NULL);



SELECT a.Name
FROM Employee a, Employee b
WHERE a.ManagerId = b.Id AND a.Salary > b.Salary ;