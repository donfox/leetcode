-- 182.sql
-- Duplicate Emails
-- Write a SQL query to find all duplicate emails in a table named Person. 
--
DROP TABLE IF EXISTS person2 ;
CREATE TABLE person2 (
    id INT,
    Email VARCHAR(20)
) ;

INSERT INTO person2 (id, Email)
             VALUES (1, 'a@b.com'),
                    (2, 'c@d.com'),
                    (3, 'a@b.com');

SELECT Email 
FROM Person2 
GROUP BY Email HAVING Count(Email) > 1;